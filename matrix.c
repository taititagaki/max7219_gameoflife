/*
	Author: James Tait
	E-mail: cjamestait@gmail.com
	License: MIT
	
	Intent: This is an implementation of the game of life using two
	MAX7219 chips controlling a 16x8 LED matrix over the GPIO pins of a
	raspberry PI.  The NAX7219 are cascaded on a Keyes DOT Matrix Shield.
	This was primarily used as a hands-on demonstration of Artificial
	Intelligence algorithms at Humber.
	
	Notes: Because the sample code found off the shop site had errors,
	coding was started from scratch with the resources below.
	Although the code was initially concieved as a library, time
	constraints and gauging a library's overall usefulness kept the
	project straightforward.
	
	Resources:
	MAX7219 Datasheet
	https://datasheets.maximintegrated.com/en/ds/MAX7219-MAX7221.pdf
	
	BCM2835 C Library for Raspberry PI by Mike McCauley (mikem@airspayce.com)
	http://www.airspayce.com/mikem/bcm2835/
*/

#include <bcm2835.h>

#define uchar unsigned char
#define uint unsigned int
#define mtxPinCS RPI_GPIO_P1_24 // Works for B+ v2 only

// Pending Buffer (New Matrix Layout)
uchar mtxpbuffer1[8] = {0,0,0,0,0,0,0,0};
uchar mtxpbuffer2[8] = {0,0,0,0,0,0,0,0};

// Frame Buffer (Current Matrix Layout)
uchar mtxfbuffer1[8] = {0,0,0,0,0,0,0,0};
uchar mtxfbuffer2[8] = {0,0,0,0,0,0,0,0};


// Delay execution for s milliseconds
void mtxDelayms(uint s)
{
	bcm2835_delay(s);
}

// Write to both chips with separate data.
void mtxWriteRaw(uchar addr1, uchar data1, uchar addr2, uchar data2)
{
	bcm2835_gpio_write(mtxPinCS, LOW);
	bcm2835_spi_transfer(addr1);
	bcm2835_spi_transfer(data1);
	bcm2835_spi_transfer(addr2);
	bcm2835_spi_transfer(data2);
	bcm2835_gpio_write(mtxPinCS, HIGH);
	mtxDelayms(10);
}

//  Mirror parameters to both chips.
void mtxWriteMirror(uchar addr, uchar data)
{
	mtxWriteRaw(addr,data,addr,data);
}

// Write to selected chip
void mtxWriteChip(uchar chip, uchar addr, uchar data)
{
	if (chip == 1)
		mtxWriteRaw(addr, data, 0x00, 0x00);
	else
		mtxWriteRaw(0x00, 0x00, addr, data);
}

// Clear pending buffer
void mtxClear(void)
{
	int i;
	for (i = 0 ; i <= 7 ; i++)
	{
		mtxpbuffer1[i] = 0x00;
		mtxpbuffer2[i] = 0x00;
	}
}

// Swap Buffer and Draw w/ orientation
void mtxDraw(void)
{
	int ti;

	// Copy Pending Bufffers into Frame Buffers
	for (ti = 0 ; ti <= 7 ; ti++)
	{
		mtxfbuffer1[ti] = mtxpbuffer1[ti];
		mtxfbuffer2[ti] = mtxpbuffer2[ti];

		mtxpbuffer1[ti] = 0x00;
		mtxpbuffer2[ti] = 0x00;
	}

	// Write Frame Buffer.  Chip Orientation with GPIO on top is 2,1.
	for (ti = 0 ; ti <= 7 ; ti++)
		mtxWriteRaw(ti+1, mtxfbuffer2[ti], ti+1, mtxfbuffer1[ti]);
}

// Set bitfield x to 1 in mtxpbuffer[y]
void mtxSetxy(uint x, uint y)
{
	if (x >= 7)
	{
		x -= 7;
		mtxpbuffer2[7-y] ^= (-1 ^ mtxpbuffer2[7-y]) & (1 << x);
	}
	else
	{
		mtxpbuffer1[7-y] ^= (-1 ^ mtxpbuffer1[7-y]) & (1 << x);
	}
}

// Clear bitfield x at correct pbuffer[y]
void mtxClearxy(uint x, uint y)
{
	if (x >= 7)
	{
		x -=7;
		mtxpbuffer2[7-y] &= ~((1) << ((x)));
	}
	else
	{
		mtxpbuffer1[7-y] &= ~((1) << ((x)));
	}
}

// Get value of bitfield x at correct pbuffer[y]
uchar mtxGetxy(uint x, uint y)
{
	uchar i;

	if (x >= 7)
	{
		x -= 7;
		i = mtxfbuffer2[7-y] >> ((x)) & 1;
	}
	else
	{
		i = mtxfbuffer1[7-y] >> ((x)) & 1;
	}

	return i;
}


// Initialize SPI, set default settings to bcm2835 and max7219, and blank screen
int mtxInit(void)
{
	if (!bcm2835_init())
		return 0;
	bcm2835_spi_begin();
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
	bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_256);
	bcm2835_gpio_fsel(mtxPinCS, BCM2835_GPIO_FSEL_OUTP);
	mtxDelayms(10);

	mtxWriteMirror(0x09, 0x00);
	mtxWriteMirror(0x0a, 0x03);
	mtxWriteMirror(0x0b, 0x07);
	mtxWriteMirror(0x0c, 0x01);
	mtxWriteMirror(0x0f, 0x00);

	mtxClear();
	mtxDraw();

	return 1;
}

//  Shutdown spi connection 
void mtxShutdown(void)
{
	bcm2835_spi_end();
	bcm2835_close();
}

// Return how many living neighbours a cell has
uint mtxNeighbours(uint x, uint y)
{
	uint n;
	n=0;

	n = mtxGetxy(x-1, y-1) > 0 ? n+1 : n;
	n = mtxGetxy(x-1, y) > 0 ? n+1 : n;
	n = mtxGetxy(x-1, y+1) > 0 ? n+1 : n;
	n = mtxGetxy(x, y-1) > 0 ? n+1 : n;
	n = mtxGetxy(x, y+1) > 0 ? n+1 : n;
	n = mtxGetxy(x+1, y-1) > 0 ? n+1 : n;
	n = mtxGetxy(x+1, y) > 0 ? n+1 : n;
	n = mtxGetxy(x+1, y+1) > 0 ? n+1 : n;

	return n;
}

int main(void)
{
	uint i, j, n;
	uchar a;
	
	a = 0;
	n = 0;

	if (!mtxInit())
		return 0;

	// Set Initial Condition: Single Glider //
	mtxSetxy(8,4);
	mtxSetxy(9,4);
	mtxSetxy(10,4);
	mtxSetxy(10,5);
	mtxSetxy(9,6);


	mtxDraw();
	mtxDelayms(100);
	// End Conditions //

	while (1)
	{
		for (i = 0 ; i <= 15 ; i++)
		{
			for (j = 0 ; j <= 7 ; j++)
			{
				a = mtxGetxy(i, j);
				n = mtxNeighbours(i,j);
				if (a > 0 && n < 2)
				{
					mtxClearxy(i, j);
				}
				else
				if (a > 0 && (n == 2 || n == 3))
				{
					mtxSetxy(i, j);
				}
				else
				if (a == 0 && n == 3)
				{
					mtxSetxy(i,j);
				}
				else
				if (a > 0 && n > 3)
				{
					mtxClearxy(i, j);
				}

			}
		}
		mtxDraw();
		mtxDelayms(1000);
	}

	mtxShutdown();
	return 1;
}
