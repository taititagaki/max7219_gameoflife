# Game of Life Matrix

## Author
James Tait-Itagaki, <james@taititagaki.ca>

## Description 
The physical part of a presentation on Artificial Intelligence Algorithms at Humber College in 2016. A glider is spawned in a static location and will move across the wrapped screen.  By pressing a button you will briefly pause normal execution and spawn one of 3 [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) shapes (still-life, oscillator, and glider) at a random location.

## Resources
### MAX7219 Datasheet
<https://datasheets.maximintegrated.com/en/ds/MAX7219-MAX7221.pdf>

### BCM2835 C Library for Raspberry PI by Mike McCauley (mikem@airspayce.com)
<http://www.airspayce.com/mikem/bcm2835/>

## License

MIT License.
